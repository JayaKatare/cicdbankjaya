/*
Klassen Bank ska innehålla en lista med alla kunder.
Klassen ska innehålla ett antal metoder som hanterar kunder och dess konton.

The Bank class must contain a list of all customers.
The class should include a number of methods that manage customers and their accounts.
*/

using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAssignment
{
    public class Bank

    {   public Database Context { get; set; }
        public Bank(Database context) 
           

        {
            Context = context;
        }
        public Bank()
        {
        }

        public List<Customer> customerList;


        // Läser in text filen och befolkar listan som ska innehålla kunderna.
        // Loads the text file and populates the list that should contain the customers.
        public virtual void Load(string path)
        {
            customerList = new List<Customer>();
            string[] lines = System.IO.File.ReadAllLines(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            List<string> list = new List<string>();
            foreach (string line in lines)
            {
                list = line.Split(':').ToList();

                string personalNumber = list[2];
                AddCustomer(list[1], personalNumber);
                Customer customer = GetCustomer(personalNumber);
                
                customer.customerAccounts = new List<Account>();

                int index = 3;
                while (index < list.Count-1) {
                    Account account = new Account()
                    {
                        accountNumber = int.Parse(list[index]),
                        accountType = list[index+1],
                        balance = float.Parse(list[index + 2], CultureInfo.InvariantCulture.NumberFormat)
                    };
                    customer.customerAccounts.Add(account);
                    index += 3;
                }
            }
        }

        // Returnerar bankens alla kunder (personnummer och namn).
        // Returns all the bank's customers (social security number and name).
        public virtual List<Customer> GetCustomers()
        {
            return this.customerList;
        }


        /*
        Skapar en ny kund med namn och personnummer. Kunden skapas endast om det inte finns någon kund med personnumret som angetts. Returnerar True om kunden skapades annars returneras False.
        Creates a new customer with name and social security number. The customer is only created if there is no customer with the social security number entered. Returns True if the customer was created otherwise False is returned.
         */
        public virtual bool AddCustomer(string Name, string PersonalNumber)
        {
            bool successfull = false;
            Customer cust = new Customer();
            cust.firstName = Name;
            cust.personalNumber = PersonalNumber;
            this.customerList.Add(cust);
            return successfull;
        }

        public virtual Customer GetCustomer(string PersonalNumber)
        {
            Customer resultCustomer = null;
            foreach (Customer customer in this.customerList)
            {
                if (customer.personalNumber == PersonalNumber)
                {
                    resultCustomer = customer;
                    break;
                }
            }
            return resultCustomer;
        }

        /*
        Returnerar information om kunden inklusive dennes konton. Första platsen i listan är förslagsvis reserverad för kundens namn och personnummer sedan följer informationen om kundens konton.
         Returns information about the customer including his accounts.The first place in the list is suggested to be reserved for the customer's name and social security number, then the information about the customer's accounts follows.
         */
        
        public virtual List<string>GetCustomerInfo(string PersonalNumber)
        {
            List<string> customerInfo = new List<string>();
            Customer customer = GetCustomer(PersonalNumber);
            customerInfo.Add(customer.firstName);
            customerInfo.Add(customer.personalNumber);
            foreach (Account account in customer.customerAccounts)
            {
                customerInfo.Add(account.ToString());
            }
            return customerInfo;
        }
        /*
        Byter namn på kund, returnerar True om namnet ändrades annars returnerar det False (om kunden inte fanns).
        Renames customer, returns True if the name was changed otherwise it returns False(if the customer did not exist).
         */
        public virtual bool ChangeCustomerName(string Name, string PersonalNumber)
        {
            bool successful = false;
            Customer customer = GetCustomer(PersonalNumber);
            if (customer != null)
            {
                customer.firstName = Name;
                successful = true;
            }
            return successful;
        }

        /*
        Tar bort kund med personnumret som angetts ur banken, alla kundens eventuella konton tas också bort och resultatet returneras. Listan som returneras ska innehålla information om alla konton som togs bort, saldot som kunden får tillbaka.
        Deletes customer with the social security number entered from the bank, all customer's possible accounts are also deleted and the result is returned. The list that is returned must contain information about all accounts that were deleted, the balance that the customer gets back.
         */
        public virtual void RemoveCustomer(string PersonalNumber)
        {
            Customer customer = GetCustomer(PersonalNumber);
            customerList.Remove(customer);
        }

        /*
        Skapar ett konto till kunden med personnumret som angetts, returnerar kontonumret som det skapade kontot fick alternativt returneras –1 om inget konto skapades.
        Creates an account for the customer with the social security number specified, returns the account number that the created account was allowed to return or -1 if no account was created.
         */
        public virtual int AddAccount(string PersonalNumber)
        {
            Customer customer = GetCustomer(PersonalNumber);

            int accountCode = -1;
            if (PersonalNumber != "")
            {
                Random rnd = new Random();
                accountCode = rnd.Next(1000, 1999);
                customer.customerAccounts.Add(new Account() { accountNumber = accountCode, accountType = "debit", balance = 0});
            }
            return accountCode;
        }

        public virtual Account GetAccount(string PersonalNumber, int AccountId)
        {
            Customer customer = GetCustomer(PersonalNumber);
            Account gottenAccount = null;

            if (customer != null)
            {
                foreach (Account account in customer.customerAccounts)
                {
                    if (account.accountNumber == AccountId)
                    {
                        gottenAccount = account;
                        break;
                    }
                }
            }

            return gottenAccount;
        }

        /*
        Returnerar Textuell presentation av kontot med kontonummer som tillhör kunden (kontonummer, saldo, kontotyp).
        Returns Textual presentation of the account with account number belonging to the customer (account number, balance, account type).
         */
        public virtual string GetAccountInfo(string PersonalNumber, int AccountId)
        {
            string accountDetails = "";
            return accountDetails;
        }

        // Gör en insättning på kontot, returnerar true om det gick bra annars false.
           // Make a deposit to the account, returns true if it went well otherwise false.

        public virtual bool Deposit(string PersonalNumber, int AccountId, float Amount)
        {
            Account account = GetAccount(PersonalNumber, AccountId);
            bool successfull = false;
            if (account != null)
            {
                account.balance += Amount;
                successfull = true;
            }
            return successfull;
        }

        // Gör ett uttag på kontot, returnerar true om det gick bra annars false.
        // Make a withdrawal on the account, returns true if it went well otherwise false.
        public virtual bool Withdraw(string PersonalNumber, int AccountId, float Amount)
        {
            bool successfull = false;
            return successfull;
        }

        // Avslutar ett konto. Textuell presentation av kontots saldo ska genereras och returneras.
        // Closes an account. Textual presentation of the account balance must be generated and returned.
        public virtual string CloseAccount(string PersonalNumber, int AccountId)
        {
           string firstName = Context.getCustomerByPersonalNumber(PersonalNumber).customerAccounts[0].balance.ToString();
          return firstName;
                                  
           //return ""; 
           
        }

    }
}
