/*

Man ska till exempel kunna ändra kundens namn samt hämta information om kunden (personnummer, för- och efternamn samt hämta information om kundens konton (kontonummer, saldo, kontotyp)). 
Dessutom ska man kunna hantera kundens konto(n). Implementera metoder som säkerställer ovanstående krav i klassen Bank nedan.
(Bank inkluderar förslag på metoder. Komplettera dessa med fler metoder om det behövs).

Translation results
For example, you must be able to change the customer's name and retrieve information about the customer (social security number, first and last name and retrieve information about the customer's accounts (account number, balance, account type)).
In addition, you must be able to manage the customer's account (s). Implement methods that ensure the above requirements in the Bank class below.
(Bank includes suggestions for methods. Supplement these with more methods if necessary).
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAssignment
{// Customer_test class tests Customer class.
    public class Customer
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string surName { get; set; }
        public string personalNumber { get; set; }

        public List<Account> customerAccounts;
    }
}

