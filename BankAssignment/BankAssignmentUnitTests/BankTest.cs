﻿using Xunit;
//Inorder to be able to reach/communicate with the program code
// inour test we need to 
// 1)Add a referance to the dependencies ( by right clicking and choosing Add referrance)
// 2)Add a using statemnt as below
using BankAssignment;

namespace BankAssignmentUnitTests
{
    public class BankTest
    {
        [Fact]
        public void GetCustomer()
        //Arrange - Given
        Bank bank = new Bank();
        Bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
       
        //Act - When
        string personalNumber = "19860107"
        Customer actual = Bank.GetCustomer(personalNumber);

       //Assert -Then
        Assert.IsType<Customer>(actual);
    } 
}
        

       