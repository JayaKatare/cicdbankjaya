﻿using Xunit;
/*In order to be able to reach/communicate with the program code
 in our test we need to 
 1)Add a referance to the dependencies(by right clicking and choosing Add referrance)
 2)Add a using statement as below
*/
using BankAssignment;

namespace BankAssignmentUnitTests
{

   
    public class Account_Test
    {
        [Trait("Account", "Get")]
        [Fact]
        public void ToStringAccount()
        { // Arrange
            Account account = new Account();

            string tostringExpected = "0  0";

            // Act
            string tostringActual = account.ToString();

            // Assert

            Assert.Equal(tostringExpected, tostringActual);
        }

        [Trait("Account", "Get")]
        [Fact]
        public void ToStringAccountWithValues()
        { // Arrange
            Account account = new Account();
            account.accountNumber = 4;
            account.accountType = "Debit";
            account.balance = 5000;

            string tostringExpected = "4 Debit 5000";

            // Act
            string tostringActual = account.ToString();

            // Assert

            Assert.Equal(tostringExpected, tostringActual);
        }
    }
}


