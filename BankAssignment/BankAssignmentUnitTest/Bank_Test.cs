﻿
/*In order to be able to reach/communicate with the program code
 in our test we need to 
 1)Add a referance to the dependencies(by right clicking and choosing Add referrance)
 2)Add a using statement as below
*/
using Xunit;
using Xunit.Abstractions;//for Mock 
using BankAssignment;
using System;
using Moq;
using System.Collections.Generic;


namespace BankAssignmentUnitTests
{
    public class BankFixture
    {
        public Bank Bank => new Bank();

    }
    public class Bank_Test : IClassFixture<BankFixture>
    {

        private ITestOutputHelper helper;
        private BankFixture bankFixture;

        private const int CUSTOMER_ID = 0;
        static MockRepository mockRepository = new MockRepository(MockBehavior.Strict);
        public Mock<Database> Context = mockRepository.Create<Database>();
        public Mock<Bank> MockBank = mockRepository.Create<Bank>();
        public Bank_Test(ITestOutputHelper testOutputHelper, BankFixture bankFixtureParameter)
        {
            helper = testOutputHelper;
            bankFixture = bankFixtureParameter;
        }

        /*1. Bank_test class tests Bank class functions.
          2.[Fact] is an anotation that we put above a test in order for the 
            program to recognise it as a test.
          3.Annotation [Theory] [InlineData] is used to perform similar tests
          4.Grouping of unit test done via [Trait] attribute */

        /* 5.Arrange Act and Arrange are the anotomy of unit tests.
            Arrange or Given (expected) is the part where we create the circumstances 
            under which the test is to be taken place
            Act or When (Actual) is the p art where we run the function we want to test. 
            Assert or Then  is the part where we confirm if the test is pased or not    */

        /*6.TestOutputHelper is created to write more information about tests in tests
             by creating private variable and then using the constructor to obtain it.
          7.Mock test:using Xunit.Abstractions; statement is used , Database.cs created 
          8.Edgecase being tested
          9.Fixture:Already created Fixture used for different tests.  */

        [Trait("Customer", "Add")]
        [Fact]
        public void AddACustomerInListAndGetCustomer()
        {
            //Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            string expectedFirstName = "Manuelll";
            string expectedPersonalNumber = "19760313";

            //Act
            // Adding 4th customer in database
            bank.AddCustomer(expectedFirstName, expectedPersonalNumber);

            Customer actual = bank.GetCustomer(expectedPersonalNumber);

            //Assert
            Assert.NotNull(actual);

            Assert.Equal(expectedFirstName, actual.firstName);
            Assert.Equal(expectedPersonalNumber, actual.personalNumber);

        }

        // Add a new customer with a unique social security number or Add one specific customer

        [Trait("Customer", "Add")]
        [Fact]
        public void AddNewCustomerForUniquePersonalNumber()
        {

            //Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            //Act
            string name = "Arvind";
            string PersonalNumber = "20000101"; //unique and new personal number 
            bool actual = bank.AddCustomer(name, PersonalNumber);

            //Assert
            Customer expected = bank.GetCustomer(PersonalNumber);
            Assert.Equal(expected.firstName, name);
            Assert.Equal(expected.id, CUSTOMER_ID);
        }

        // Add a new customer with a duplicate social security number.
        // Add customers when  Bool is set to False

        [Trait("Customer", "Add")]
        [Fact]
        public void AddCustomerforDuplicatePersonalNumbershouldNotAdd()
        {

            //Arrange 
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            //Act
            string name = "Arvind";
            string PersonalNumber = "20000101";// personal number of already exited (Edge case being tested)
            bool actual = bank.AddCustomer(name, PersonalNumber);

            //Assert
            Customer expected = bank.GetCustomer(PersonalNumber);
            Assert.False(actual);
        }

        // Add new customers using [InlineData]

        [Trait("Customer", "Add")]
        [Theory]
        [InlineData("Jaya", "19981221")]
        [InlineData("Abhi", "19981220")]
        [InlineData("Nive", "19981221")]
        public void AddInlineCustomers(string name, string PersonalNumber)
        {
            //Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            //Act
            bool changeInName = bank.AddCustomer(name, PersonalNumber);

            //Assert
            Customer expected = bank.GetCustomer(PersonalNumber);
            Assert.Equal(expected.firstName, name);
            Assert.Equal(expected.id, CUSTOMER_ID);
        }

        // Get already existed customer
        [Trait("Customer", "Get")]
        [Fact]
        public void GetCustomerWhoAlreadyExist()
        {
            // Arrange 
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            //Act 
            string personalNumber = "19860107";
            Customer actual = bank.GetCustomer(personalNumber);

            //Assert 
          
            Assert.IsType<Customer>(actual);
        }

        // Check Customer who doesnt exists in bank

        [Trait("Customer", "Get")]
        [Fact]
        public void GetCustomerByIncorrectPersonalNumber()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            // Act
            string personalNumber = "19861221"; // 19861221 personal number does not exists in data
            Customer actual = bank.GetCustomer(personalNumber);

            // Assert
            Assert.Null(actual); 
        }

        // Get the list of customers
        [Trait("Customer", "Get")]
        [Fact]
        public void GetCustomers()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            // Act
            List<Customer> actual = bank.GetCustomers();

            // Assert
            Assert.IsType<List<Customer>>(actual);
            Assert.True(actual.Count > 1);
        }

        // Get customer information who exist
        [Trait("Customer", "Get")]
        [Fact]
        public void GetCustomerInfo()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string firstName = "Linnea";
            string accountDetail = "1003 debit 0";

            // Act
            string personalNumber = "19860107";
            List<string> actual = bank.GetCustomerInfo(personalNumber);

            // Assert
            Assert.Equal(actual[0], firstName);
            Assert.Equal(actual[2], accountDetail);
        }

        // Change a customer's name (social security number should not be able to change).
        // Change existing customer name
        [Trait("Customer", "Change")]
        [Fact]
        public void ModifyExistingCustomerName()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19860107";
            string newName = "Linneaaa";

            // Act
            bool actual = bank.ChangeCustomerName(newName, personalNumber);
            Customer customer = bank.GetCustomer(personalNumber);

            helper.WriteLine("Changed the existed customer name to " + newName + " for personal number " + personalNumber + ".");

            // Assert
            Assert.True(actual);
            Assert.Equal(newName, customer.firstName);

        }

        // Change Customer name who dont exist
        [Trait("Customer", "Change")]
        [Fact]
        public void ChangeAnotherCustomerName()

        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19981231";
            string newName = "Indhu";

            // Act
            bool actual = bank.ChangeCustomerName(newName, personalNumber);

            // Assert
            Assert.False(actual);
        }

        // Deleting an existing customer, concerned account must also be closed.
        [Trait("Customer", "Remove")]
        [Fact]
        public void RemoveCustomer()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            string personalNumber = "19860107";
            int actualCustomerCount = 2;


            // Act
            bank.RemoveCustomer(personalNumber);
            List<Customer> customers = bank.GetCustomers();
            Customer customer = bank.GetCustomer("19860107");  // An existing customer
            helper.WriteLine("Removed the customer with personal Number " + personalNumber + " from the Customer list.");

            // Assert
            Assert.Equal(customers.Count, actualCustomerCount);
            Assert.Null(customer);

        }
        [Trait("Customer", "Remove")]
        [Fact]
        public void RemoveCustomerWithWrongPersonalNumber()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19811111"; // Incorrect Personal Number
            int actualCustomerCount = 3;
            

            // Act
            bank.RemoveCustomer(personalNumber);
            List<Customer> customers = bank.GetCustomers();
            Customer customer = bank.GetCustomer("19811111"); //

            // Assert
            Assert.Equal(customers.Count, actualCustomerCount);
            Assert.Null(customer);
        }

        // Create account (Account) for an existing customer, a unique account number is generated.

        [Trait("Account", "Add")]
        [Fact]
        public void AddAccount()
        { // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19860107";

            // Act
            int accountNumber = bank.AddAccount(personalNumber);
            helper.WriteLine("Account added" + accountNumber + " for personal number " + personalNumber + ".");

            // Assert
            Assert.IsType<int>(accountNumber);
            Assert.False(accountNumber < 999);  // EdgeCase being tested
            Assert.False(accountNumber > 2000);  // EdgeCase being tested
        }

        // Get account with correct accountId
        [Trait("Account", "Get")]
        [Fact]
        public void GetAccount()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19860107";
            int accountId = 1003;
            string accountType = "debit";

            // Act
            Account account = bank.GetAccount(personalNumber, accountId);

            // Assert
            Assert.Equal(account.accountNumber, accountId);
            Assert.Equal(account.accountType, accountType);
        }

        // GetAccount with wrong accountId
        [Trait("Account", "Get")]
        [Fact]
        public void GetAccountInfo()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19860107";
            int accountId = 1212121; // (ID not available in data.txt)
            string accountDetails = "";

            // Act
            string accountDet = bank.GetAccountInfo(personalNumber, accountId);

            // Assert
            Assert.Equal(accountDetails, accountDet);
        }

        //-----------------------------------Transaction----------------------------------------------------
        // Deposit amount using correct personal Number and accountId
        [Trait("Transaction", "Deposite")]
        [Fact]
        public void DepositAmount()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19860107";
            int accountId = 1003;
            float amount = 2;

            // Act
            bool success = bank.Deposit(personalNumber, accountId, amount);

            // Assert
            Assert.True(success);
        }

        //Deposit amount using correct personal Number and incorrect accountId
        [Trait("Transaction", "Deposite")]
        [Fact]
        public void DepositFailForWrongAccountId()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19860107";
            int accountId = 1000; //Incorrect accountId
            float amount = 2;

            // Act
            bool success = bank.Deposit(personalNumber, accountId, amount);

            // Assert
            Assert.False(success);
        }

        // Withdraw money from the account where accountID dosent match
        [Trait("Transaction", "Withdraw")]
        [Fact]
        public void WithdrawMoney()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");
            string personalNumber = "19860107";
            int accountId = 1004; //Account does not exist in data.txt
            float amount = 2;

            // Act
            bool success = bank.Withdraw(personalNumber, accountId, amount);

            // Assert
            Assert.False(success);
        }
        [Trait("Transaction", "Withdraw")]
        [Fact]
        public void WithdrawMoneyfloat()
        {
            // Arrange
            Bank bank = bankFixture.Bank;
            bank.Load(@"C:\Testvertyg\Unit testing\assignment 2 _Bank\VTTestJensen22-main\data.txt");

            string personalNumber = "19760314";
            int accountId = 1005; 
            float amount = 201; // Balance amount is 200  (Edge case being tested)

            // Act
            bool success = bank.Withdraw(personalNumber, accountId, amount);

            // Assert
            Assert.False(success);
        }

        //-----------------------------------Mock----------------------------------------------------


        //Withdraw money from the account using mock

        [Trait("Mock", "Withdraw")]
        [Fact]
        public void mockWithdrawAmount()
        {

            Bank bank = MockBank.Object;
            Customer customer = new Customer();
            customer.firstName = "Arvind";
            customer.personalNumber = "20010202";

            int balance = 700;
            int toWithdraw = 300;

            customer.customerAccounts = new List<Account>();

            customer.customerAccounts.Add(new Account() { accountNumber = 13245, accountType = "Debit", balance = balance });

            MockBank.Setup(mock => mock.Withdraw("20010202", 13245, toWithdraw)).Returns (true);

            //Act 
                                 
            bool withdrawMoney = bank.Withdraw("20010202", 13245, toWithdraw);

            int expected = balance - toWithdraw;
            int actual = (int)customer.customerAccounts[0].balance;

            // Assert
            Assert.True(withdrawMoney);

        }
               
        [Trait("Mock", "CloseAccount")]
        [Fact]
        public void mockCloseAccount()
        {
            // Arrange

            Bank bank = new Bank(Context.Object);
            Customer customer = new Customer();
            customer.firstName = "Arvind";
            customer.personalNumber = "20010202";

            customer.customerAccounts = new List<Account>();

            customer.customerAccounts.Add(new Account() { accountNumber = 13245, accountType = "Debit", balance = 1000 });

            Context.Setup(mock => mock.getCustomerByPersonalNumber("20010202")).Returns(customer);
            string expected = customer.customerAccounts[0].balance.ToString();

            //Act 
            string closeAccount = bank.CloseAccount("20010202", 13245);

            // Assert
            Assert.Equal(expected, closeAccount);
        }
        [Trait("Mock", "CloseAccount")]
        [Fact]
        public void mockCloseAccount1()
        {
            // Arrange

            Bank bank = MockBank.Object;
            Customer customer = new Customer();
            customer.firstName = "Arvind";
            customer.personalNumber = "20010202";

            customer.customerAccounts = new List<Account>();

            customer.customerAccounts.Add(new Account() { accountNumber = 13245, accountType = "Debit", balance = 1000});

            MockBank.Setup(mock => mock.CloseAccount("20010202", 13245)).Returns("");
            string expected = "";

            //Act 

            string closeAccount = bank.CloseAccount("20010202", 13245);
                  
            // Assert
            Assert.Equal(expected, closeAccount);

        }
    }
}
